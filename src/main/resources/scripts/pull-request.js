define('plugin/issue-enforcer/web-panel', [
	'jquery',
	'aui',
	'stash/api/util/navbuilder',
	'exports'
	], 
	function (
		$,
		AJS,
		nav,
		exports) {

		// Regular expressions
		var PROJECT_KEY_RE = 	/\/projects\/([\w-]+)\//,
			REPO_SLUG_RE = 		/\/repos\/([\w-]+)\//,
			PULLREQUEST_ID_RE =	/\/pull-requests\/(\d+)\//,
			COMMIT_HASH_RE = 	/ ([a-z0-9]{11})/,
			ISSUE_RE = 			/ ([A-Z]+\-\d+)/;	

		function getMessage() {
			try {
				$.ajax({
					url: getUrlBase(),
					contentType: 'application/json',
					dataType: "json",
					data: {
						projectKey: getProjectKey(),
						repoSlug: getRepoSlug(),
						pullRequestId: getPullRequestId()
					},
					success: function(res) {
						if(!res || res.length === 0 || res[0] == null) {
							return;
						}
						loadWarning(res);
					},
					timeout: 15000, // 15 seconds,
					error: function(jqxhr, status, err) {
						if(status === "timeout") {
							var warning = $("#check-warning");
							warning.html('The Jira Issue Enforcer plugin took too long to complete. '
								+ 'There are probably many commits in this pull request. '
								+ 'It is up to you to determine if any are missing an issue reference or exemption keyword.'
								+ getConfluenceLinkMessage());
							warning.css("padding-bottom", "15px");
						}
					}
				});
			} catch (err) {
				console.log(err);
			}
		}

		function getConfluenceLinkMessage() {
			return '</br><b>Consult <a href="https://confluence.teslamotors.com/pages/viewpage.action?pageId=20946408">commit message policy</a> for instructions to resolve.</b>';
		}

		function loadWarning(res) {
			var warning = $("#check-warning");
			warning.html(buildWarningMessage(res));
			warning.css("padding-bottom", "15px");

			var r = Math.floor((Math.random() * 50) + 1);
			var gifSrc = gifSrc = "http://www.animated-gifs.eu/transportation-signage-stop/0020.gif";
			// Not everyone is mature enough to enjoy this.
			//if(r == 1) {
			//	gifSrc = "http://weknowmemes.com/wp-content/uploads/2012/07/holly-ellenbogen-stop-sign.gif";
			//}
			warning.prepend('<img style="margin-left: auto; margin-right: auto; display:block;" id="stopsign"' +
				' src="' + gifSrc + '" alt="STOP" /></br>');
		}

		function buildWarningMessage(res) {
			var msg = res[0];
			var resClone = []
			var missingRefs = [];

			// Combine missing ref messages
			for(var i = 0; i < res.length; i++) {
				if(i != 0 && res[i].indexOf('does not contain') != -1) {
					var ref = res[i].match(COMMIT_HASH_RE);
					missingRefs.push(ref[0]);
				} else {
					resClone.push(res[i]);
				}
			}

			if(missingRefs.length > 1) {
				res = resClone;
				msg += ' These commits do not contain an issue reference in the log message: ';
				var max = missingRefs.length < 6 ? missingRefs.length : 6;
				msg += modifyText(missingRefs[0]);
				for(var i = 1; i < max; i++) {
					msg += ', ' + modifyText(missingRefs[i]);
				}
				if(missingRefs.length > 6) {
					msg += '... and ' + (missingRefs.length - 6) + ' more similar issues.';
				}
			}

			var max = res.length < 4 ? res.length : 4;
			for(var i = 1; i < max; i++) {
				msg += '</br>' + modifyText(res[i]) + " ";
			}
			msg = msg.trim();
			if(max == 4 && res.length != 4) { // Truncate longer messages
				if(msg[msg.length-1] == ".") {
					msg += "..";
				}
				else {
					msg += "...";
				}
				msg += " and " + (res.length - 4) + " more similar issues.";
			}
			msg += getConfluenceLinkMessage();
			return msg;
		}

		// Link and red bold commit hashes and issues
		function modifyText(text) {
			var hash = text.match(COMMIT_HASH_RE)[1];
			var url = nav.project(getProjectKey()).repo(getRepoSlug()).commit(hash).buildAbsolute();
			var str = text.replace(COMMIT_HASH_RE, ' <a href="' + url + '"><i style="color:red; text-decoration:underline">$1</i></a>');
			str = str.replace(ISSUE_RE, ' <i style="color:red;">$1</i>.');
			return str;
		}

		function reloadWarning() {
			$(document).ready(function() {
				// Make overview tab reload warning on click
				var tabs = $("ul.tabs-menu a");
				tabs[0].onclick = function() {
					getMessage();
				};
			});
		}

		function getProjectKey() {
			return window.location.pathname.match(PROJECT_KEY_RE)[1];
		}

		function getRepoSlug() {
			return window.location.pathname.match(REPO_SLUG_RE)[1];
		}

		function getPullRequestId() {
			return window.location.pathname.match(PULLREQUEST_ID_RE)[1];
		}

		function getUrlBase() {
			return AJS.contextPath() + '/rest/issue-check/1.0/';
		}

		exports.onReady = function() {
			getMessage();
			reloadWarning();
		};
	}
);

require('plugin/issue-enforcer/web-panel').onReady();