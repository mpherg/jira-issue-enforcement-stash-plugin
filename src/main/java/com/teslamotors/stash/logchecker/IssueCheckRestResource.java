package com.teslamotors.stash.logchecker;

import java.util.Collection;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.hook.repository.RepositoryHookService;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.pull.PullRequestService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.user.EscalatedSecurityContext;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.user.SecurityService;
import com.atlassian.bitbucket.util.Operation;

@Path("/")
public class IssueCheckRestResource {

	private final CommitService commitService;
	private final ApplicationPropertiesService applicationPropertiesService;
	private final ApplicationLinkService applicationLinkService;
	private final RepositoryService repositoryService;
	private final PullRequestService pullRequestService;
	private final RepositoryHookService repositoryHookService;
	private final TransactionTemplate transactionTemplate;
	private final SecurityService securityService;

	public IssueCheckRestResource(
			ApplicationPropertiesService applicationPropertiesService,
            CommitService commitService,
			ApplicationLinkService applicationLinkService,
			RepositoryService repositoryService,
			PullRequestService pullRequestService,
			RepositoryHookService repositoryHookService,
			TransactionTemplate transactionTemplate,
			SecurityService securityService) {
		this.commitService = commitService;
		this.applicationPropertiesService = applicationPropertiesService;
		this.applicationLinkService = applicationLinkService;
		this.repositoryService = repositoryService;
		this.pullRequestService = pullRequestService;
		this.repositoryHookService = repositoryHookService;
		this.transactionTemplate = transactionTemplate;
		this.securityService = securityService;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context HttpServletRequest request) {
		try {
			String projectKey = request.getParameter("projectKey");
			String repoSlug = request.getParameter("repoSlug");		
			String pullRequestStr = request.getParameter("pullRequestId");
			final Repository repo = repositoryService.getBySlug(projectKey, repoSlug);
			final PullRequest pullRequest = pullRequestService.getById(repo.getId(), Long.parseLong(pullRequestStr));
			EscalatedSecurityContext secContext = securityService.withPermission(Permission.REPO_ADMIN, "Become repo admin to retrieve plugin settings.");
			final Settings settings = (Settings) secContext.call(new Operation<Settings, Throwable>() {
	
					@Override
					public Settings perform() throws Throwable {
						return repositoryHookService.getSettings(repo, "com.teslamotors.stash.hook.jira-issue-enforcer:commit-message-issue-enforcer");
					}
					
				});
			if(settings.getBoolean(LogMessageHookConfig.CHECK_WARNING_ONLY_ENABLED_SOY_FIELD, false)) {
				return Response.ok(transactionTemplate.execute(new TransactionCallback<Collection<String>>() {
					@Override
					public Collection<String> doInTransaction() {
						Collection<String> errorLines;
						try {
							errorLines = CommitLogMessagePreReceiveHook.checkForIssues(
									settings,
									pullRequest,
									applicationLinkService,
									applicationPropertiesService,
									commitService);
						} catch (Exception e) {
							errorLines = Collections.singleton(e.getMessage());
						}
						return errorLines;
					}
					
				})).build();
			} else {
				return Response.noContent().build();
			}
			
		} catch (Throwable e1) {
			return Response.serverError().build();
		}
	}
}
