package com.teslamotors.stash.logchecker;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.application.jira.JiraApplicationType;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.bitbucket.commit.Commit;
import com.google.common.collect.Sets;
import com.teslamotors.stash.logchecker.LogMessageHookConfig.JiraConnection;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.JSONValue;

// ========================================================================
/**
 * The jira-rest-java-client (https://ecosystem.atlassian.net/wiki/display/JRJC/Home)
 * could not be loaded by the Stash classloader, for 
 * some reason, so we will have to interact with the Jira rest services at a lower level.
 * See https://docs.atlassian.com/jira/REST/5.2/
 */
public class JiraIssueUtils {

	final static String ISSUE_PATTERN_STRING = "([A-Z]+)-(\\d+)";
	public final static Pattern JIRA_ISSUE_PATTERN = Pattern.compile(ISSUE_PATTERN_STRING);
	public final static Pattern NONEXSTENT_ISSUE_ERROR_PATTERN = Pattern.compile("An issue with key '(" + ISSUE_PATTERN_STRING + ")' does not exist");
	final static Pattern START_MERGE_COMMIT_MESSAGE_PATTERN = Pattern.compile("^\\* .*:");

	// ========================================================================
	static Object getQueryFromApplicationLink(ApplicationLinkService applicationLinkService, String full_query_path) throws Exception {

		ApplicationLink application_link = applicationLinkService.getPrimaryApplicationLink(JiraApplicationType.class);
		if (application_link == null)
			throw new Exception("ERROR: Administrator has not yet set up Jira Application Link!");

		ApplicationLinkRequest req = application_link.createAuthenticatedRequestFactory().createRequest(Request.MethodType.GET, 
				full_query_path);

		ApplicationLinkResponseHandler<Object> handler = new ApplicationLinkResponseHandler<Object>() {

			@Override
			public Object credentialsRequired(Response response)
					throws ResponseException {
				return null;
			}

			@Override
			public Object handle(Response response)
					throws ResponseException {
				return JSONValue.parse(new InputStreamReader(response.getResponseBodyAsStream()));
			}
		};
		try {
			return req.execute(handler);
		} catch(ResponseException e) {
			System.out.println(e.getMessage());
		}
		return new Object();
	}
	
	// ========================================================================
	public static String getQueryString(Map<String, String> query_parameters) {
		if (query_parameters.isEmpty()) {
			return "";
		} else {
			List<NameValuePair> params = new LinkedList<NameValuePair>();
			for (Entry<String, String> pair : query_parameters.entrySet())
				params.add(new BasicNameValuePair(pair.getKey(), pair.getValue()));

			return "?" + URLEncodedUtils.format(params, "utf-8");
		}
	}

	// ========================================================================
	public static Object getJiraQueryJson(JiraConnection jira_connection, String api_command, Map<String, String> query_parameters) throws Exception {

		final String query_string = getQueryString(query_parameters);
		String full_query_path = "/rest/api/latest/" + api_command + query_string;

		if (!jira_connection.use_jira_application_link) {
		    HttpClient httpClient = HttpClientBuilder.create().build();

            UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(jira_connection.jira_username, jira_connection.jira_password);

            String query_url = jira_connection.jira_base_url + full_query_path;
            HttpGet httpGet = new HttpGet(query_url);
            BasicScheme scheme = new BasicScheme(Charset.defaultCharset());
            httpGet.addHeader(scheme.authenticate(credentials, httpGet, null));

            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity responseEntity = httpResponse.getEntity();
            InputStream input_stream = responseEntity.getContent();

			return (Object) JSONValue.parse(new InputStreamReader(input_stream));
		} else {
			return (Object) getQueryFromApplicationLink(jira_connection.applicationLinkService, full_query_path);
		}
	}

	// ========================================================================
    /** Finds JIRA issues of the format ([A-Z]+)-(\\d+) in commit messages.
     *  This method ignores everything after a line that starts with "*" and
     *  contains ":" to ignore all commit messages listed in the summary of
     *  a merge commit.
     *  
     *  When parsing commit messages for JIRA references, one needs to take care not to
     *  parse the merge commit summary. Here we stop parsing the merge commit message
     *  once we reach the beginning of the merge summary. This line always starts
     *  with a "* " (start then space) and also contains a colon. Some examples are:
     * "* commit 'f8970fa558fc57...7e1d9a5407d435046abde':"
     * "* commit '89709be943d538a402dd215b5d9b0e32ccf79c1c': (23 commits)"
     * "* 'develop/2014.3.29' of ssh://stash.teslamotors.com:7999/FW/firmware:"
     * @param commit
     * @return
     */
    static Set<String> findIssues(Commit commit) {

        Set<String> jira_issues = Sets.newHashSet();
        String[] message_lines = commit.getMessage().split("\n");
        for (String message : message_lines) {
            Matcher start_merge_commit_message = START_MERGE_COMMIT_MESSAGE_PATTERN.matcher(message);
            if (start_merge_commit_message.find()) {
                break;
            }
            Matcher jira_issue_matcher = JIRA_ISSUE_PATTERN.matcher(message);

            while (jira_issue_matcher.find())
                jira_issues.add( jira_issue_matcher.group() );
        }
        return jira_issues;
    }

    // ========================================================================
    /** Finds JIRA issues of the format ([A-Z]+)-(\\d+) in commit messages and
     *  filters them against a provided set of valid JIRA project keys. Only
     *  issue references that contain a valid project key will be returned.
     *  This method uses the findIssues method to to get the set of issues to
     *  perform key filtering on.
     * @param commit
     * @param valid_project_keys
     * @return
     */
    static Set<String> findIssuesWithValidProjectKeys(Commit commit, Set<String> valid_project_keys) {

        Set<String> issue_references_with_valid_project_keys = Sets.newHashSet();
        Set<String> unfiltered_issue_references = findIssues(commit);

        for (String issue_reference : unfiltered_issue_references) {
           Matcher issue_key_matcher = JIRA_ISSUE_PATTERN.matcher(issue_reference);
           if (issue_key_matcher.find()) {
               String project_key = issue_key_matcher.group(1);
               if (valid_project_keys.contains(project_key)) {
                   issue_references_with_valid_project_keys.add(issue_reference);
               }
           }
        }
        return issue_references_with_valid_project_keys;
    }

    // ========================================================================
    /** Queries the JIRA REST API for data about all projects and returns the
     *  set of all project keys.
     *
     *  WARNING: The JIRA API doc states that this API "Returns a list of
     *  projects for which the user has the BROWSE, ADMINISTER or PROJECT_ADMIN
     *  project permission." This method may return an incomplete set of project
     *  keys depending on the user's permissions.
     * @param jira_connection
     * @return
     * @throws Exception
     */
    public static Set<String> getProjectKeys(JiraConnection jira_connection) throws Exception {

        JSONArray jira_project_objects = (JSONArray) getJiraQueryJson(jira_connection, "project", new HashMap<String, String>());

        Set<String> project_keys = Sets.newHashSet();

        for (Object project_object : jira_project_objects) {
            JSONObject jira_project = (JSONObject) project_object;
            project_keys.add((String) jira_project.get("key"));
        }

        return project_keys;
    }
}
