package com.teslamotors.stash.logchecker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nonnull;

import org.eclipse.jgit.lib.Constants;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitCallback;
import com.atlassian.bitbucket.commit.CommitContext;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.CommitSummary;
import com.atlassian.bitbucket.commit.CommitsBetweenRequest;
import com.atlassian.bitbucket.hook.HookResponse;
import com.atlassian.bitbucket.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.hook.repository.RepositoryMergeRequestCheck;
import com.atlassian.bitbucket.hook.repository.RepositoryMergeRequestCheckContext;
import com.atlassian.bitbucket.pull.PullRequest;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.RefChangeType;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.setting.Settings;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

public class CommitLogMessagePreReceiveHook implements PreReceiveRepositoryHook, RepositoryMergeRequestCheck {

    private final CommitService commitService;
    private final ApplicationPropertiesService applicationPropertiesService;
    private final ApplicationLinkService applicationLinkService;

    public CommitLogMessagePreReceiveHook(
            ApplicationPropertiesService applicationPropertiesService,
            CommitService commitService,
            ApplicationLinkService applicationLinkService) {
        this.commitService = commitService;
        this.applicationPropertiesService = applicationPropertiesService;
        this.applicationLinkService = applicationLinkService;
    }

    // ========================================================================
    /**
     * Streams commits between provided ref_change.
     * @return Collection<Commit> commits between ref_change
     */
    static Collection<Commit> getCommitsForRefchange(
            Repository repository,
            RefChange ref_change,
            CommitService commitService) {

        Collection<Commit> commits_for_refchange = Lists.newArrayList();

        CommitsBetweenRequest request = new CommitsBetweenRequest.Builder(repository)
        		.exclude(ref_change.getFromHash())
        		.include(ref_change.getToHash())
        		.build();

        CommitCallback callback = new CommitCallback() {
			@Override
			public boolean onCommit(Commit commit) throws IOException {
				commits_for_refchange.add(commit);
				return true;
			}
			@Override
			public void onEnd(CommitSummary summary) throws IOException { /* unused */ }
			@Override
			public void onStart(CommitContext context) throws IOException { /* unused */ }
        };

        commitService.streamCommitsBetween(request, callback);

        return commits_for_refchange;
    }

    // ========================================================================
    static Map<Commit, CommitValidationSummary> enforceIssueReferencesOnAllRefs(
            Map<RefChange, Iterable<Commit>> commits_by_refchange,
            LogMessageHookConfig hook_config,
            CommitService commitService,
            ApplicationLinkService applicationLinkService) throws Exception {

        // XXX For performance reasons, this object is NOT immediately populated.
        IssueExistenceResult issue_existence_result = new IssueExistenceResult();

        Set<String> valid_project_keys = JiraIssueUtils.getProjectKeys(hook_config.jira_connection);

        Map<Commit, CommitValidationSummary> issue_references_by_commit_id = Maps.newHashMap();
        for (Entry<RefChange, Iterable<Commit>> entry : commits_by_refchange.entrySet()) {
            Iterable<Commit> commits = entry.getValue();
            for (Commit commit : commits) {
                Set<String> issues = JiraIssueUtils.findIssuesWithValidProjectKeys(commit, valid_project_keys);

                CommitValidationSummary summary = new CommitValidationSummary(
                        issues,
                        issue_existence_result,
                        commit);

                issue_references_by_commit_id.put(commit, summary);
            }
        }

        // We do all issue references in one batch for speed
        Set<String> all_issue_references = Sets.newHashSet();
        for (CommitValidationSummary summary : issue_references_by_commit_id.values())
            all_issue_references.addAll(summary.issue_references);

        if (!all_issue_references.isEmpty() && hook_config.validate_issue_references_against_jira) {
            // XXX This throws an exception of any of the issue references do not exist in Jira
            // XXX We discard this reference here, because we have already stowed a reference
            // to this object in each CommitValidationSummary.
            issue_existence_result.populateIssueMovesAndNonexistence(
                    applicationLinkService,
                    all_issue_references,
                    hook_config);
        }

        return issue_references_by_commit_id;
    }

    // ========================================================================
    public static Collection<String> checkRefsForRejection(
            Collection<RefChange> ref_changes,
            WatershedCommitFilter watershed_commit_filter,
            LogMessageHookConfig hook_config,
            Repository repository,
            CommitService commitService,
            ApplicationLinkService applicationLinkService) {

        // Here we can ignore certain refs
        Map<RefChange, Iterable<Commit>> commits_by_refchange = Maps.newHashMap();
        for (RefChange ref_change : ref_changes) {

            if (RefChangeType.UPDATE != ref_change.getType())
                continue;

            if (!hook_config.isRefMatchedByPrefix(ref_change))
                continue;

            Collection<Commit> commits = getCommitsForRefchange(
                  repository,
                  ref_change,
                  commitService);

            ArrayList<Commit> filtered_commits = new ArrayList<Commit>();
            try {
                for (Commit commit : commits) {
                    if (watershed_commit_filter.isPastWatershed(commit)) {
                        filtered_commits.add(commit);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            commits_by_refchange.put(ref_change, filtered_commits);
        }

        watershed_commit_filter.cleanup();

        List<String> hook_response_error_lines = Lists.newArrayList();
        try {
            Map<Commit, CommitValidationSummary> validation_summary_by_commit = enforceIssueReferencesOnAllRefs(
                    commits_by_refchange,
                    hook_config,
                    commitService,
                    applicationLinkService);

            for (Entry<RefChange, Iterable<Commit>> refchange_entry : commits_by_refchange.entrySet()) {

                String shortened_branch_name;
                if(refchange_entry.getKey() instanceof PullRequestRefChange)
                    shortened_branch_name = ((PullRequestRefChange)refchange_entry.getKey()).getFromRefId().substring(Constants.R_HEADS.length());
                else
                    shortened_branch_name = refchange_entry.getKey().getRef().getId().substring(Constants.R_HEADS.length());

                List<String> aggregated_commit_errors_for_branch = Lists.newArrayList();
                for (Commit commit : refchange_entry.getValue()) {
                    CommitValidationSummary summary = validation_summary_by_commit.get(commit);
                    aggregated_commit_errors_for_branch.addAll(summary.getCommitErrors(hook_config));
                }

                if (!aggregated_commit_errors_for_branch.isEmpty()) {
                    hook_response_error_lines.add( String.format("Error(s) in branch \"%s\":", shortened_branch_name) );
                    for (String commit_error : aggregated_commit_errors_for_branch)
                        hook_response_error_lines.add( '\t' + commit_error );
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            hook_response_error_lines.add( e.getMessage() );
        }

        return hook_response_error_lines;
    }

    public static Collection<String> checkForIssues(Settings settings, PullRequest pullRequest,
            ApplicationLinkService applicationLinkService,
            ApplicationPropertiesService applicationPropertiesService,
            CommitService commitService) throws Exception {
        LogMessageHookConfig hook_config = LogMessageHookConfig.fromSettings(settings, applicationLinkService);
        Repository repo = pullRequest.getToRef().getRepository();

        WatershedCommitFilter watershed_commit_filter;
        watershed_commit_filter = WatershedCommitFilter.create(applicationPropertiesService,
                repo,
                hook_config);

        RefChange ref_change = new PullRequestRefChange( pullRequest );
        return CommitLogMessagePreReceiveHook.checkRefsForRejection(
                Collections.singleton(ref_change),
                watershed_commit_filter,
                hook_config,
                repo,
                commitService,
                applicationLinkService);
    }

    // ========================================================================
    @Override
    public boolean onReceive(@Nonnull RepositoryHookContext repo_context, @Nonnull Collection<RefChange> ref_changes,
            @Nonnull HookResponse hook_response) {

        LogMessageHookConfig hook_config = LogMessageHookConfig.fromSettings(repo_context.getSettings(),
                this.applicationLinkService);

        WatershedCommitFilter watershed_commit_filter;
        try {
            watershed_commit_filter = WatershedCommitFilter.create(this.applicationPropertiesService,
                    repo_context.getRepository(), hook_config);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        Collection<String> error_lines = checkRefsForRejection(ref_changes, watershed_commit_filter, hook_config,
                repo_context.getRepository(), this.commitService, this.applicationLinkService);

        if (repo_context.getSettings().getBoolean(LogMessageHookConfig.CHECK_WARNING_ONLY_ENABLED_SOY_FIELD, false)) {
            return true;
        } else {
            for (String error_line : error_lines) {
                hook_response.err().println(error_line);
            }
            return error_lines.isEmpty();
        }
    }


    @Override
    public void check(RepositoryMergeRequestCheckContext context) {
        boolean warnOnly = context.getSettings().getBoolean(LogMessageHookConfig.CHECK_WARNING_ONLY_ENABLED_SOY_FIELD, false);
        try {
            Collection<String> error_lines = checkForIssues(context.getSettings(), context.getMergeRequest().getPullRequest(),
                    this.applicationLinkService,
                    this.applicationPropertiesService,
                    this.commitService);
            if (!error_lines.isEmpty() && !warnOnly){
                context.getMergeRequest().veto(String.format("Rejected by merge hook \"%s\"", this.getClass().getSimpleName()), Joiner.on('\n').join(error_lines));
            }
        } catch (Exception e) {
            e.printStackTrace();
            if(!warnOnly) {
                context.getMergeRequest().veto(String.format("Error in merge hook \"%s\"", this.getClass().getSimpleName()), e.getMessage());
            }
            return;
        }
    }
}
