package com.teslamotors.stash.logchecker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Matcher;

import com.atlassian.applinks.api.ApplicationLinkService;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

// ========================================================================
/**
 * This is the only class that does any network interaction with Jira.
 */
public class IssueExistenceResult {
	public static final int MAX_THREAD_COUNT = 2;
	public static final long WAIT_PERIOD_MS = 100;
	public static final long REQUEST_TIMEOUT_MS = 30 * 1000L; // 30 seconds
	public static final double MAX_QUERY_SIZE = 50.0;
	public final Set<String> nonexistent_issues = Sets.newHashSet();
	public final Map<String, String> moved_issue_map = Maps.newHashMap();

	private ExecutorService pool;

	@SuppressWarnings("unchecked")
	public void populateIssueMovesAndNonexistence(
			ApplicationLinkService applicationLinkService,
			final Collection<String> issues,
			LogMessageHookConfig config) throws Exception {

		Set<String> validated_issue_references = Sets.newHashSet();
		pool = Executors.newFixedThreadPool(MAX_THREAD_COUNT);

		// Split queries into chunks for performance
		List<List<String>> issueChunks = splitIssues(issues);

		Collection<String> first_query_error_messages = new ArrayList<>();

		Collection<JSONObject> firstResults = new ArrayList<>();
		// Don't create a new thread if there is only one chunk
		if(issueChunks.size() == 1) {
			firstResults.add(sendJiraSearchRequest(issueChunks.get(0), config));
		} else {
			firstResults.addAll(sendRequestsAsync(issueChunks, config));
		}

		for(JSONObject result : firstResults) {
			if (result.containsKey("errorMessages")) {
				first_query_error_messages.addAll((Collection<String>) result.get("errorMessages"));
			}
		}

		Collection<String> unaccounted_error_messages = Lists.newArrayList();
		for (String error_message : first_query_error_messages) {
			Matcher m = JiraIssueUtils.NONEXSTENT_ISSUE_ERROR_PATTERN.matcher(error_message);
			if (m.find())
				this.nonexistent_issues.add(m.group(1));
			else
				unaccounted_error_messages.add(error_message);
		}

		if (!unaccounted_error_messages.isEmpty())
			throw new Exception(Joiner.on('\n').join(unaccounted_error_messages));

		if (!nonexistent_issues.isEmpty()) {

			final Collection<String> issues_without_errors = new HashSet<String>(issues);
			issues_without_errors.removeAll(nonexistent_issues);

			List<List<String>> errorFreeIssueChunks = splitIssues(issues_without_errors);

			// Query for all issues that don't have an error
			Collection<JSONObject> errorFreeResults = new ArrayList<>();

			// Don't create a new thread if there is only one chunk
			if(errorFreeIssueChunks.size() == 1) {
				errorFreeResults.add(sendJiraSearchRequest(errorFreeIssueChunks.get(0), config));
			} else {
				errorFreeResults.addAll(sendRequestsAsync(errorFreeIssueChunks, config));
			}

			for(JSONObject result : errorFreeResults) {
				JSONArray issues_array = (JSONArray) result.get("issues");
				if(issues_array != null) {
					for (Object issue_object : issues_array) {
						JSONObject json_issue_object = (JSONObject) issue_object;
						String issue_key = (String) json_issue_object.get("key");
						validated_issue_references.add(issue_key);
					}
				}
			}

			// Check if there are issues referencing projects that do not exist
			Collection<String> issues_without_projects = new HashSet<String>(issues);
			issues_without_projects.removeAll(validated_issue_references);
			issues_without_projects.removeAll(nonexistent_issues);
			if (!issues_without_projects.isEmpty()) {
				this.nonexistent_issues.addAll(issues_without_projects);
			}
		}

		// Find moved issues
		// This might be slow, because it performs one Jira query per issue.
		if (config.check_for_issue_move) {
			for (String nonexistent_issue : this.nonexistent_issues) {

				JSONObject moved_query_json_object = (JSONObject) JiraIssueUtils.getJiraQueryJson(config.jira_connection, "issue/" + nonexistent_issue, new HashMap<String, String>());
				if (moved_query_json_object.containsKey("errorMessages")) {

					Collection<String> moved_query_error_messages = (Collection<String>) moved_query_json_object.get("errorMessages");
					for (String message : moved_query_error_messages) {
						if ("Issue Does Not Exist".equalsIgnoreCase(message)) {
							// Issue does not exist and was not moved.
						}
					}

				} else {
					this.moved_issue_map.put(nonexistent_issue, (String) moved_query_json_object.get("key"));
				}
			}
		}

		pool.shutdown();
	}

	/*
	 * Split a collection of issues into smaller chunks
	 */
	private List<List<String>> splitIssues(final Collection<String> issues) {
		List<List<String>> issueChunks = new ArrayList<>();

		// Turn the issues into a list so it can be indexed
		List<String> issuesList = new ArrayList<>();
		issuesList.addAll(issues);

		int numChunks = (int) Math.ceil(issues.size() / MAX_QUERY_SIZE);
		for(int chunkIndex = 0; chunkIndex < numChunks; chunkIndex++) {
			List<String> chunk = new ArrayList<>();
			for(int issueIndex = (int) (chunkIndex * MAX_QUERY_SIZE); issueIndex < Math.min((chunkIndex + 1) * MAX_QUERY_SIZE, issues.size()); issueIndex++) {
				chunk.add(issuesList.get(issueIndex));
			}

			issueChunks.add(chunk);
		}

		return issueChunks;
	}

	/*
	 * Send issue chunks separately
	 */
	private Collection<JSONObject> sendRequestsAsync(List<List<String>> issueChunks, LogMessageHookConfig config)
			throws InterruptedException, ExecutionException {

		Collection<JSONObject> results = new ArrayList<>();
		List<Future<JSONObject>> futures = new ArrayList<>();

		for(List<String> chunk : issueChunks) {
			// Create task to be executed with this chunk
			Future<JSONObject> future = pool.submit(new Callable<JSONObject>() {
				@Override
				public JSONObject call() throws Exception {
					return sendJiraSearchRequest(chunk, config);
				}
			});

			futures.add(future);
		}

		// Wait for all threads to finish and add their results, equivalent to join
		for(Future<JSONObject> future : futures) {
			try {
				results.add(future.get(REQUEST_TIMEOUT_MS, TimeUnit.MILLISECONDS));
			} catch (TimeoutException e) {
				e.printStackTrace();
			}
		}

		return results;
	}

	@SuppressWarnings("serial")
	private JSONObject sendJiraSearchRequest(List<String> issues, LogMessageHookConfig config) throws Exception {
		return (JSONObject) JiraIssueUtils.getJiraQueryJson(config.jira_connection, "search", new HashMap<String, String>() {{
			put("fields", "summary");
			put("jql", "issue in (" + Joiner.on(',').join(issues) + ")");
		}});
	}

}
